import React, {Component} from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {NavHeader} from "../../common/component/nav-header";
import {Categories} from "./categories";

export class Home extends Component {
    render() {
        return (
            <View style={styles.container}>
                <NavHeader
                    {...{
                        onLeftBtnPress: () => console.log(123),
                        leftBtnIcon: (<Image source={require('../../assets/images/nav-left.png')}/>),
                        title: (<Image source={require('../../assets/images/Centrum-Horizontal.png')}/>)
                    }}
                />

                <Categories {...this.props}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
});
