import React, {Component} from 'react';
import {Image, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {LatestNews} from "./latest-news/latest-news";
import {EvilIcons} from '@expo/vector-icons'

let categories = [
    {
        category: "Latest News",
        items: [
            {
                title: "Get an Oil Change While you work",
                imageUri: require("../../assets/images/image.png")
            },
            {
                title: "Get an Oil Change While you work",
                imageUri: require("../../assets/images/image.png")
            },
            {
                title: "Get an Oil Change While you work",
                imageUri: require("../../assets/images/image.png")
            },
        ]
    },
    {
        category: "Perks",
        items: [
            {
                title: "Get an Oil Change While you work",
                imageUri: require("../../assets/images/image.png")
            },
            {
                title: "Get an Oil Change While you work",
                imageUri: require("../../assets/images/image.png")
            },
            {
                title: "Get an Oil Change While you work",
                imageUri: require("../../assets/images/image.png")
            },
        ]
    },
    {
        category: "Events",
        items: [
            {
                title: "Get an Oil Change While you work",
                imageUri: require("../../assets/images/image.png")
            },
            {
                title: "Get an Oil Change While you work",
                imageUri: require("../../assets/images/image.png")
            },
            {
                title: "Get an Oil Change While you work",
                imageUri: require("../../assets/images/image.png")
            },
        ]
    },
    {
        category: "Buildings",
        items: [
            {
                title: "Get an Oil Change While you work",
                imageUri: require("../../assets/images/image.png")
            },
            {
                title: "Get an Oil Change While you work",
                imageUri: require("../../assets/images/image.png")
            },
            {
                title: "Get an Oil Change While you work",
                imageUri: require("../../assets/images/image.png")
            },
        ]
    },
    {
        category: "Polls",
        items: [
            {
                title: "Get an Oil Change While you work",
                imageUri: require("../../assets/images/image.png")
            },
            {
                title: "Get an Oil Change While you work",
                imageUri: require("../../assets/images/image.png")
            },
            {
                title: "Get an Oil Change While you work",
                imageUri: require("../../assets/images/image.png")
            },
        ]
    },
];

export class Categories extends Component {
    render() {
        const {navigation} = this.props;

        return (
            <ScrollView style={styles.container}>
                {categories.map((c, idx) => (
                    <View style={styles.category} key={idx}>
                        <View style={styles.header}>
                            <Text style={styles.categoryTitle}>{c.category.toUpperCase()}</Text>

                            <View style={styles.readMoreContainer}>
                                <TouchableOpacity onPress={() => navigation.navigate("LatestNews")}>
                                    <View style={styles.readMore}>
                                        <Text style={styles.readMoreText}>View More</Text>
                                        <EvilIcons name={"chevron-right"} size={30} color="#F27B28"/>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>

                        <ScrollView
                            horizontal
                            showsHorizontalScrollIndicator={false}
                        >
                            {c.items.map((item, key) => (
                                <View key={key} style={styles.item}>
                                    <Image source={item.imageUri}/>
                                    <Text style={styles.itemTitle}>{item.title}</Text>
                                </View>
                            ))}
                        </ScrollView>
                    </View>
                ) )}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        paddingLeft: 20,
        paddingTop: 17,
        paddingBottom: 5,
        backgroundColor: "#fff"
    },
    category: {
        marginBottom: 15
    },
    header: {
        position: "relative",
        marginBottom: 15
    },
    readMoreContainer: {
        position: "absolute",
        right: 10
    },
    readMore: {
        flexDirection: "row",
        alignItems: "center",
    },
    readMoreText: {
        color: "#F27B28"
    },
    categoryTitle: {
        fontSize: 18,
        fontFamily: "pingfang-regular"
    },
    item: {
        marginRight: 15,
        maxWidth: 130
    },
    itemTitle: {
        marginTop: 5,
        fontFamily: "pingfang-semibold",
        color: "#8F8E94",
        fontSize: 12
    }
});
