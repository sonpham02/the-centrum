import React, {Component} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {NavHeader} from "../../../common/component/nav-header";
import {EvilIcons} from '@expo/vector-icons'
import {SafeAreaView} from "react-navigation";

let item = {
    imageUri: require("../../../assets/images/image1.png"),
    title: "Get an oil change while you work",
    author: "Honeycomb Team",
    time: "05 MAY 2018 15:30",
    description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
};

export class LatestNewsDetail extends Component {
    render() {
        const {navigation} = this.props;
        return (
            <View style={styles.container}>
                <NavHeader
                    {...{
                        onLeftBtnPress: () => navigation.goBack(),
                        leftBtnIcon: (<EvilIcons name={"chevron-left"} size={40}/>),
                        title: (<Text style={{fontSize: 18, fontFamily: "pingfang-semibold"}}>LATEST NEWS</Text>)
                    }}
                />

                <View>
                    <Image source={item.imageUri}/>
                    <View style={styles.info}>
                        <Text style={styles.title}>{item.title.toUpperCase()}</Text>

                        <View style={styles.articleInfo}>
                            <Text style={styles.author}>{item.author}</Text>
                            <Text style={styles.time}>{item.time}</Text>
                        </View>

                        <Text style={{lineHeight: 22}}>
                            {item.description}
                        </Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff"
    },
    info: {
        padding: 18
    },
    title: {
        fontSize: 24,
        color: "#F27B28",
        fontFamily: "pingfang-light"
    },
    articleInfo: {
        flexDirection: "row",
        marginTop: 10,
        marginBottom: 20
    },
    author: {
        fontSize: 10,
        color: "#000" ,
        marginRight: 5
    },
    time: {
        fontSize: 10,
        color: "#B7B7B7"
    },
    readMore: {
        flexDirection: "row",
        alignItems: "center",
        borderTopWidth: 1,
        borderColor: "#EFEFF4",
        paddingTop: 5,
        marginTop: 10
    }
});
