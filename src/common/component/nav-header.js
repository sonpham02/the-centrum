import React, {Component} from 'react';
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';

export class NavHeader extends Component {
    render() {
        const {onLeftBtnPress, leftBtnIcon, title} = this.props;

        return (
            <View style={styles.navHeader}>
                <View style={styles.leftHeader}>
                    <TouchableOpacity onPress={onLeftBtnPress}>
                        {leftBtnIcon}
                    </TouchableOpacity>
                </View>

                <View style={styles.title}>
                    {title}
                </View>

                <View style={styles.rightHeader}>
                    <Image source={require('../../assets/images/icon-profile.png')}/>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    navHeader: {
        flexDirection: "row",
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        borderColor: "#E3E3E3",
        borderBottomWidth: 1,
        backgroundColor: "#fff"
    },
    leftHeader : {
        position: "absolute",
        left: 20
    },
    title: {

    },
    rightHeader: {
        position: "absolute",
        right: 20
    }
});
