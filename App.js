import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Home} from "./src/js/home/home";
import {Font} from 'expo';
import {createStackNavigator, SafeAreaView} from 'react-navigation';
import {LatestNews} from "./src/js/home/latest-news/latest-news";
import {LatestNewsDetail} from "./src/js/home/latest-news/latest-news-detail";

const RootStack = createStackNavigator(
    {
        Home:             { screen: Home },
        LatestNews:       { screen: LatestNews},
        LatestNewsDetail: { screen: LatestNewsDetail}
    },
    {
        headerMode: "none",
        cardStyle: { shadowColor: 'transparent' },
        initialRouteName: "Home"
    }
);

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fontLoaded: false
        }
    }

    async componentDidMount() {
        await Font.loadAsync({
            'pingfang-regular': require('./src/assets/fonts/PingFang-SC-Regular.ttf'),
            'pingfang-bold': require('./src/assets/fonts/PingFang-SC-Bold.ttf'),
            'pingfang-semibold': require('./src/assets/fonts/PingFang-SC-Semibold.ttf'),
            'pingfang-light': require('./src/assets/fonts/PingFang-SC-Light.ttf'),
        });

        this.setState({fontLoaded: true})
    }

    render() {
        if(!this.state.fontLoaded) return <View/>;
        return (
            <SafeAreaView style={styles.container}>
                <RootStack/>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        // alignItems: 'center',
        // justifyContent: 'center',
    },
});


